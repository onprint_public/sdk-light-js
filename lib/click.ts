/**
 *
 */

import * as utils from './utils';
import axios, { AxiosRequestConfig } from 'axios';
import { ApiHeader } from '.';

export function clickAction(actionId: string, sessionId: string, headers: ApiHeader): Promise<any> {
    const uri = utils.OPRINT_API_URL + '/api/Clicks?actionId=' + actionId;
    const config: AxiosRequestConfig = {
        method: 'PUT',
        headers: {
            ...headers,
            SessionId: sessionId
        },
        validateStatus: utils.validateStatus
    };
    return axios(uri, config);
}
