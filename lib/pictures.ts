/**
 * @file Function wrapper for Onprint Picture API
 * @author Benoit Martins <benoit@ltutech.com>
 */
import * as utils from './utils';
import axios, { AxiosRequestConfig } from 'axios';
import { ApiHeader } from '.';

/** The API Endpoint for Picture requests */
const picturesEndpoint = '/api/pictures/';

/**
 * Interface for wrapping Onprint API Response
 * @interface PictureObject
 */
export interface PictureObject {
    /** Picture Id used by Onprint */
    id: string;
    /** Picture Name */
    name?: string;
    /** Picture data, usualy in JSON format but can contain any data format */
    data?: string;
    /** Score returned by the matching algo */
    score: string;
    /** Organization Id where the Picture is stored */
    orgId: string;
    /** Url of the Picture image */
    thumbUrl: string;
    /** SessionId used to fill the reporting */
    sessionId: string;
    /**  An array of Keywords used to improve matching */
    keywords?: string[];
}

/**
 * Call onprint API to get Picture from an existing ImageId
 * @export
 * @param {string} id Picture Id
 * @param {ApiHeader} headers set to call Onprint API
 * @returns {Promise<any>}
 */
export function searchImageById(id: string, headers: ApiHeader): Promise<any> {
    const uri: string = utils.OPRINT_API_URL + picturesEndpoint + id;
    const config: AxiosRequestConfig = {headers, validateStatus: utils.validateStatus};
    return axios.get(uri, config);
}

/**
 * Call onprint API to get an array of Pictures that best match the image send in argument
 * @export
 * @param {Blob} img Image we try to match with the database
 * @param {ApiHeader} headers set to call Onprint API
 * @returns {Promise<any>}
 */
export function searchImageByImage(img: Blob, headers: ApiHeader): Promise<any> {
    const uri = utils.OPRINT_API_URL + picturesEndpoint + 'search';
    const boundary = utils.getBoundary();
    const data = new Blob([
        utils.createFileContent('jo.jpg', 'file', 'image/jpeg', boundary),
        img,
        utils.createEndContent(boundary)
    ]);
    const config: AxiosRequestConfig = {
        method: 'POST',
        headers: {
            ...headers,
            'Content-Type': `multipart/form-data; boundary=${boundary}`
        },
        data,
        validateStatus: utils.validateStatus
    };
    return axios(uri, config);
}
