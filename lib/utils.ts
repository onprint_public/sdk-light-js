export const OPRINT_API_URL = 'https://api.onprint.com';

export const validateStatus = (status: number) => {
    return status < 500;
};

export function getBoundary(): string {
    return 'XXX' + (new Date()).getTime() + 'XXX';
}

export function createFileContent(fileName: string, atachName: string, fileType: string, boundary: string): string {
    let contentFile = '--' + boundary + '\r\n';
    contentFile += 'Content-Disposition: form-data; name=' + atachName + '; filename="' + fileName + '"\r\n';
    contentFile += 'Content-Type: ' + fileType + '\r\n';
    contentFile += '\r\n';
    return contentFile;
}

export function createFormContent(object: object, objName: string, boundary: string): string {
    let contentForm = '--' + boundary + '\r\n';
    contentForm += 'Content-Type: application/json; charappend=utf-8\r\n';
    contentForm += 'Content-Disposition: form-data; name=' + objName + '\r\n';
    contentForm += '\r\n';
    contentForm += JSON.stringify(object) + '\r\n';
    return contentForm;
}

export function createEndContent(boundary: string): string {
    let contentEnd = '\r\n';
    contentEnd += '--' + boundary + '--';
    return contentEnd;
}
