import {SdkLight, DevHeader, AppHeader} from '..';
import * as fs from 'fs-extra';

export const APIKEY = process.env.API_KEY || '';

describe('Test SdkLight Class', () => {
    const validDevHeader: DevHeader = {
        name: 'jsSdkTestMobile',
        version: '8.0.0',
        versionName: 'Oreo'
    };
    const validAppHeader: AppHeader = {
        name: 'JsSDKTestApp',
        version: '1.0.0',
        id: '42'
    };
    describe('Check initialiazation', () => {
        test('Vaild APIKEY', () => {
            const sdk = new SdkLight(APIKEY, validAppHeader, validDevHeader);
            expect(sdk).toBeInstanceOf(SdkLight);
        });
        test('Invaild APIKEY', () => {
            expect(() => {
                new SdkLight('', validAppHeader, validDevHeader) // tslint:disable-line
            }).toThrow('Undefined or invalid API KEY');
        });
        test('Invalid App Header: empty param id', () => {
            expect(() => {
                new SdkLight(APIKEY, {"name": "jsSDKTestApp", "version": "1.0.0", "id": ""}, validDevHeader) // tslint:disable-line
            }).toThrow('Invalid arguments type or value to set application header expect string but insted had: ' + '' + ' in arg ' + 2); // tslint:disable-line
        });
        test('Invalid App Header: param name is type number', () => {
            expect(() => {
                // @ts-ignore
                new SdkLight(APIKEY, {"name":12, "version": "1.0.0", "id": "42"}, validDevHeader) // tslint:disable-line
            }).toThrow('Invalid arguments type or value to set application header expect string but insted had: ' + 12 + ' in arg ' + 0); // tslint:disable-line
        });
        test('Invalid Dev Header: empty param name', () => {
            expect(() => {
                new SdkLight(APIKEY, validAppHeader, {"name": "", "version": "1.0.0", "versionName": "oreo"}) // tslint:disable-line
            }).toThrow('Invalid arguments type or value to set device header expect string but insted had: ' + '' + ' in arg ' + 0); // tslint:disable-line
        });
        test('Invalid Dev Header: param version is type object', () => {
            expect(() => {
                // @ts-ignore
                new SdkLight(APIKEY, validAppHeader, {"name": "jsSdkTestMobile", "version": {}, "versionName": "oreo"}) // tslint:disable-line
            }).toThrow('Invalid arguments type or value to set device header expect string but insted had: ' + {} + ' in arg ' + 1); // tslint:disable-line
        });
    });

    describe('Set Lang', () => {
        const sdk = new SdkLight(APIKEY, validAppHeader, validDevHeader);
        test('Valid lang code', () => {
            expect(sdk.setLang('fr-FR')).toBeUndefined();
        });
        test('Invalid lang format', () => {
            expect(() => {sdk.setLang('frFR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('frFR-'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('-frFR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('f-rFR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr--FR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr-FR-'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('-fr-FR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr-FR-'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr-FR-'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr_FR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr_fr'); }).toThrow('Invalid lang format');
        });
        test('Invalid lang format 2', () => {
            expect(() => {sdk.setLang('En-EN'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fR-FR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('FR-FR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr-fR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr-Fr'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('fr-FRR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('frr-FR'); }).toThrow('Invalid lang format');
            expect(() => {sdk.setLang('frr-FRR'); }).toThrow('Invalid lang format');
        });
    });

    describe('getEnrichedImageById', () => {
        const sdk = new SdkLight(APIKEY, validAppHeader, validDevHeader);
        test('Valid request', async () => {
            const enImg = await sdk.getEnrichedImageById('b9ceb71c-aad9-4c90-80c6-4fe894de85fb');
            expect(enImg).toHaveProperty('Actions');
        });
        test('Invalid request', async () => {
            const enImg = await sdk.getEnrichedImageById('c9ceb71c-aad9-4c90-80c6-4fe894de85fb');
            expect(enImg).toBeNull();
        });
        test('missing Id', () => {
            expect(
                // @ts-ignore
                sdk.getEnrichedImageById()
            ).rejects.toThrow('Empty image id');
        });
        test('Id is number', () => {
            expect(
                // @ts-ignore
                sdk.getEnrichedImageById(12)
            ).rejects.toThrow('Image id should be a string instead was number');
        });
    });

    describe('getEnrichedImageByImage', () => {
        const sdk = new SdkLight(APIKEY, validAppHeader, validDevHeader);
        sdk.setLang('fr-FR');
        test('Get Joconde Data', async () => {
            const imgBuffer = await fs.readFile('./lib/__tests__/validEnrImage.jpeg');
            // @ts-ignore
            const enImg = await sdk.getEnrichedImageByImage(imgBuffer);
            delete enImg.SessionId;
            // tslint:disable-next-line
            expect(enImg).toMatchObject({"Actions": [{"BackgroundImage": "http://data.onprint.com/images/leonardo-da-vinci.jpg", "Content": "{\"Url\":\"http://fr.m.wikipedia.org/wiki/L%C3%A9onard_de_Vinci\",\"IsEmbeddedWebView\":true,\"IsAnonymousURL\":false,\"ContentType\":\"text/html\"}", "Icon": "https://api.onprint.com/Icons/d/dd18dbce-d52d-4bd3-832e-c89350565ea6.png", "Id": "011bf905-7988-4ab7-ae0b-64b339fc5b8e", "Name": "À propos de Leonard de Vinci", "Order": 2, "Priority": 1, "Style": "{\"model\":\"ONPrint-V2.0\",\"button\":{\"background-color\":\"#F56B16\",\"opacity\":\"0.6\",\"radius\":\"0\",\"height\":\"3\",\"image-size\":\"contain\",\"image-position\":\"right\"},\"text\":{\"font-weight\":\"normal\",\"font-style\":\"normal\",\"color\":\"#ffffff\",\"opacity\":1,\"vposition\":\"bottom\",\"hposition\":\"left\"},\"icon\":{\"vposition\":\"top\",\"hposition\":\"left\",\"size\":\"64\"}}", "Type": "URL"}, {"BackgroundImage": "http://data.onprint.com/images/logo-louvre.jpg", "Content": "{\"Url\":\"http://www.louvre.fr\",\"IsEmbeddedWebView\":true,\"IsAnonymousURL\":false,\"ContentType\":\"text/html\"}", "Icon": "https://api.onprint.com/Icons/a/a362bf3c-47d7-4ee2-9f48-cfe0400b38a8.png", "Id": "97e8b4a7-f842-4dee-af32-4207aa644e86", "Name": "Le Louvre", "Order": 10, "Priority": -1, "Style": "{\"model\":\"ONPrint-V2.0\",\"button\":{\"display\":\"true\",\"background-color\":\"#303030\",\"opacity\":0,\"height\":\"279x82\",\"image-size\":\"contain\"},\"icon\":{\"display\":\"false\",\"vposition\":\"middle\",\"hposition\":\"left\",\"size\":\"32\"},\"text\":{\"display\":\"false\",\"color\":\"#FFFFFF\",\"opacity\":1,\"background-color\":\"rgba(255,255,255,0.0)\",\"font-weight\":\"normal\",\"font-style\":\"normal\",\"vposition\":\"middle\",\"hposition\":\"center\",\"font-size\":\"16\"}}", "Type": "URL"}, {"BackgroundImage": "http://data.onprint.com/images/mona-lisa.jpg", "Content": "{\"Url\":\"http://fr.m.wikipedia.org/wiki/La_Joconde\",\"IsEmbeddedWebView\":true,\"IsAnonymousURL\":false,\"ContentType\":\"text/html\"}", "Icon": "https://api.onprint.com/Icons/d/dd18dbce-d52d-4bd3-832e-c89350565ea6.png", "Id": "cfd3dd56-5861-4f1e-9778-47e5f2180818", "Name": "À propos de La Joconde", "Order": 1, "Priority": 1, "Style": "{\"model\":\"ONPrint-V2.0\",\"button\":{\"background-color\":\"#65828C\",\"opacity\":\"0.56\",\"height\":\"3\",\"image-position\":\"left\",\"image-size\":\"contain\"},\"text\":{\"font-weight\":\"normal\",\"font-style\":\"normal\",\"color\":\"#ffffff\",\"opacity\":1,\"vposition\":\"bottom\",\"hposition\":\"right\"},\"icon\":{\"size\":\"64\",\"vposition\":\"top\",\"hposition\":\"right\"}}", "Type": "URL"}, {"Content": "{\"Url\":\"https://www.boutiquesdemusees.fr/fr/portraits/vinci-leonard-de-la-joconde-portrait-de-monna-lisa/5295.html\",\"IsEmbeddedWebView\":true,\"IsAnonymousURL\":false,\"ContentType\":\"text/html\"}", "Icon": "https://api.onprint.com/Icons/a/aac756e2-7689-4d19-9746-76520c1a5107.png", "Id": "dee4992d-0997-4b15-9aa7-66c62ed7e74e", "Name": "Acheter l'affiche", "Order": 4, "Priority": 1, "Style": "{\"model\":\"ONPrint-V2.0\",\"button\":{\"background-color\":\"#C49B12\",\"opacity\":\"0.6\",\"radius\":\"10\"},\"text\":{\"font-weight\":\"normal\",\"font-style\":\"normal\",\"color\":\"#FFFFFF\",\"opacity\":\"1\"}}", "Type": "URL"}, {"Content": "{\"Url\":\"http://maps.google.com/maps?q=musee%20du%20louvre%20paris\",\"IsEmbeddedWebView\":true,\"IsAnonymousURL\":false,\"ContentType\":\"text/html\"}", "Icon": "https://api.onprint.com/Icons/a/a7a5835e-ae09-41e9-9e4b-f7404d38075b.png", "Id": "e99b8c22-2807-4959-a4db-d0724e7edb65", "Name": "Où se trouve l'œuvre ?", "Order": 3, "Priority": 1, "Style":"{\"model\":\"ONPrint-V2.0\",\"button\":{\"background-color\":\"#C92424\",\"opacity\":\"0.6\",\"radius\":\"10\"},\"text\":{\"font-weight\":\"normal\",\"font-style\":\"normal\",\"color\":\"#FFFFFF\",\"opacity\":\"1\"}}", "Type": "URL"}, {"Content": "{\"Url\":\"http://www.onprint.com/games/quizz/QuizzJoconde.html\",\"IsEmbeddedWebView\":true,\"IsAnonymousURL\":false,\"ContentType\":\"text/html\"}", "Icon": "https://api.onprint.com/Icons/0/09f478e0-0ff5-4c0f-89cd-271fda5ed42e.png", "Id": "f9a45d74-6e78-4768-89fb-c1e6cc7a9621", "Name": "Quizz La Joconde", "Order": 5, "Priority": 1, "Style": "{\"model\":\"ONPrint-V2.0\",\"button\":{\"background-color\":\"#CF19CF\",\"opacity\":\"0.52\",\"radius\":\"10\"},\"text\":{\"font-weight\":\"normal\",\"font-style\":\"normal\",\"color\":\"#FFFFFF\",\"opacity\":\"1\"}}", "Type": "URL"}], "AutoTrigger": false, "BigThumbnailUrl": "https://api.onprint.com/Files/Thumbs/5/6/2/56274aee-cd4b-406e-8fa7-21fa35f6618a/1c8ceccc-59d6-4515-a61f-5722455764d0/700.jpg", "DocumentId": "56274aee-cd4b-406e-8fa7-21fa35f6618a", "Id": "1c8ceccc-59d6-4515-a61f-5722455764d0", "LanguageCode": "fr-fr", "Order": 0, "ScanPrefs": {"BlackAndWhite": true, "JpegQuality": "0.8", "LongestSideSizePx": 400}, "Title": "La Joconde - Léonard de Vinci", "TitleStyle": "{\"model\":\"ONPrint-V2.0\",\"button\":{\"background-color\":\"#3c68dd\",\"opacity\":0.6},\"text\":{\"color\":\"#FFFFFF\",\"font-weight\":\"normal\",\"font-style\":\"normal\",\"opacity\":1}}"});
            return enImg;
        });
    });

    describe('clickImage', () => {
        const sdk = new SdkLight(APIKEY, validAppHeader, validDevHeader);
        sdk.setLang('fr-FR');
        test('Click On Action', async () => {
            const imgBuffer = await fs.readFile('./lib/__tests__/validEnrImage.jpeg');
            // @ts-ignore
            const enImg = await sdk.getEnrichedImageByImage(imgBuffer);
            const clicked = await sdk.clickOnAction(enImg.Actions[0].Id);
            expect(clicked).toBeTruthy();
        });
        test('Click with false actionId', async () => {
            await sdk.getEnrichedImageById('b9ceb71c-aad9-4c90-80c6-4fe894de85fb');
            const clicked = await sdk.clickOnAction('12');
            expect(clicked).toBeFalsy();
        });
        test('Click with no sessionId', async () => {
            const sdk2 = new SdkLight(APIKEY, validAppHeader, validDevHeader);
            sdk2.setLang('fr-FR');
            expect(sdk2.clickOnAction('12')).rejects.toThrow('Undefined SessionId: You should first request an Image');
        });
    });
});
