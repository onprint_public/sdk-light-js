import * as enrichedImage from '../enrichedImage';
import { APIKEY } from './index.spec';
import { ApiHeader } from '..';
import * as fs from 'fs-extra';

const validHeader: ApiHeader = {
    ApiKey: APIKEY,
    SdkVersion: '0.0.1',
    SdkName: 'Onprint SDK light JS',
    ApplicationName: 'JsSDKTestApp',
    ApplicationVersion: '1.0.0',
    ApplicationInstanceId: '42',
    DeviceName: 'jsSdkTestMobile',
    DeviceSystemVersion: '8.0.0',
    DeviceSystemVersionName: 'Oreo'
};

const unvalidHeader: ApiHeader = {
    ApiKey: '',
    SdkVersion: '0.0.1',
    SdkName: 'Onprint SDK light JS',
    ApplicationName: 'JsSDKTestApp',
    ApplicationVersion: '1.0.0',
    ApplicationInstanceId: '42',
    DeviceName: 'jsSdkTestMobile',
    DeviceSystemVersion: '8.0.0',
    DeviceSystemVersionName: 'Oreo'
};

const validImgId: string = 'b9ceb71c-aad9-4c90-80c6-4fe894de85fb';
const validImg = fs.readFileSync('./lib/__tests__/validEnrImage.jpeg');
const invalidImg = fs.readFileSync('./lib/__tests__/invalidImage.png');

describe('Test api call to enriched image', () => {
    describe('enrichedImageById', () => {
        test('valid request', async () => {
            const data = await enrichedImage.searchImageById(validImgId, 'fr-FR', validHeader);
            expect(data.status).toEqual(200);
            expect(data.data.Title).toEqual('Autruche');
        });
        test('Invalid Image Id', async () => {
            // @ts-ignore
            const data = await enrichedImage.searchImageById({key: 'value'}, 'fr-FR', validHeader);
            expect(data.status).toEqual(400);
            expect(data.data.Message).toEqual('The request is invalid.');
        });
        test('Missing ApiKey', async () => {
            // @ts-ignore
            const data = await enrichedImage.searchImageById(validImgId, 'fr-FR', unvalidHeader);
            expect(data.status).toEqual(401);
            expect(data.data).toEqual('the API key you provided does not exist');
        });
        test('Not existing Id', async () => {
            // @ts-ignore
            const data = await
                enrichedImage.searchImageById('c9ceb71c-aad9-4c90-80c6-4fe894de85fb', 'fr-FR', validHeader);
            expect(data.status).toEqual(404);
            expect(data.statusText).toEqual('Not Found');
        });
    });
    describe('EnrichedImageByImage', () => {
        test('valid request', async () => {
            // @ts-ignore
            const data = await enrichedImage.searchImageByImage(validImg, 'fr-FR', validHeader);
            const enrImg = data.data;
            expect(data.status).toEqual(200);
            expect(enrImg.Title).toEqual('La Joconde - Léonard de Vinci');
        });
        test('Missing ApiKey', async () => {
            // @ts-ignore
            const data = await enrichedImage.searchImageByImage(validImg, 'fr-FR', unvalidHeader);
            expect(data.status).toEqual(401);
            expect(data.data).toEqual('the API key you provided is not authorized: ');
        });
        test('Not existing Image', async () => {
            // tslint:disable
            // @ts-ignore
            const data = await enrichedImage.searchImageByImage(invalidImg, 'fr-FR', validHeader);
            expect(data.status).toEqual(204);
            expect(data.statusText).toEqual('No Content');
        });
    });
});
