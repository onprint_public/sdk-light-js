import * as pictures from '../pictures';
import { APIKEY } from './index.spec';
import { ApiHeader } from '..';
import * as fs from 'fs-extra';

const validHeader: ApiHeader = {
    ApiKey: APIKEY,
    SdkVersion: '0.0.1',
    SdkName: 'Onprint SDK light JS',
    ApplicationName: 'JsSDKTestApp',
    ApplicationVersion: '1.0.0',
    ApplicationInstanceId: '42',
    DeviceName: 'jsSdkTestMobile',
    DeviceSystemVersion: '8.0.0',
    DeviceSystemVersionName: 'Oreo'
};

const unvalidHeader: ApiHeader = {
    ApiKey: '',
    SdkVersion: '0.0.1',
    SdkName: 'Onprint SDK light JS',
    ApplicationName: 'JsSDKTestApp',
    ApplicationVersion: '1.0.0',
    ApplicationInstanceId: '42',
    DeviceName: 'jsSdkTestMobile',
    DeviceSystemVersion: '8.0.0',
    DeviceSystemVersionName: 'Oreo'
};

const validPictureId: string = '20da33b8-09fb-4fab-b87f-2159815159e2';
const validPicture = fs.readFileSync('./lib/__tests__/validPicture.png');
const invalidImage = fs.readFileSync('./lib/__tests__/invalidImage.png');

describe('Test api call to Picture image', () => {
    describe('PictureById', () => {
        test('valid request', async () => {
            const data = await pictures.searchImageById(validPictureId, validHeader);
            const picture = data.data;
            expect(data.status).toEqual(200);
            expect(picture.Name).toEqual('logo-ltu');
        });
        test('Invalid Image Id', async () => {
            // @ts-ignore
            const data = await pictures.searchImageById({key: 'value'}, validHeader);
            expect(data.status).toEqual(404);
            expect(data.statusText).toEqual('Not Found');
        });
        test('Missing ApiKey', async () => {
            // @ts-ignore
            const data = await pictures.searchImageById(validPictureId, unvalidHeader);
            expect(data.status).toEqual(401);
        });
        test('Not existing Id', async () => {
            // @ts-ignore
            const data = await
                pictures.searchImageById('c9ceb71c-aad9-4c90-80c6-4fe894de85fb', validHeader);
            expect(data.status).toEqual(404);
            expect(data.statusText).toEqual('Not Found');
        });
    });
    describe('PictureByImage', () => {
        test('valid request', async () => {
            // @ts-ignore
            const data = await pictures.searchImageByImage(validPicture, validHeader);
            const picture = data.data[0];
            expect(data.status).toEqual(200);
            expect(picture.Name).toEqual('logo-ltu');
        });
        test('Missing ApiKey', async () => {
            // @ts-ignore
            const data = await pictures.searchImageByImage(validPicture, unvalidHeader);

            expect(data.status).toEqual(401);
            expect(data.data).toEqual('the API key you provided is not authorized: ');
        });
        test('Not existing Image', async () => {
            // tslint:disable
            // @ts-ignore
            const data = await pictures.searchImageByImage(invalidImage, validHeader);
            expect(data.status).toEqual(404);
            expect(data.statusText).toEqual('Not Found');
        });
    });
});
