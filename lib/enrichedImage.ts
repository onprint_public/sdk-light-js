import * as utils from './utils';
import axios, { AxiosRequestConfig } from 'axios';
import { ApiHeader } from '.';

export interface EnrichedImageObject {
    id: string;
    name?: string;
    score: string;
    orgId: string;
    order: number;
    title: string;
    actions?: Action;
    langCode: string;
    thumbUrl: string;
    sessionId: string;
    documentId: string;
    titleStyle?: string;
    autoTrigger: boolean;
}
export enum ActionType {
    'SMS',
    'URL',
    'MAP',
    'DATA',
    'SHARE',
    'EMAIL',
    'PHONE',
    'VCARD',
}
export interface Action {
    id: string;
    name: string;
    type: ActionType;
    order: number;
    style?: string;
    iconUrl?: string;
    content?: string;
    priority: number;
}

const enImgEndpoint = '/api/enrichedimages/';
/**
 * Call onprint API to get image from an existing ImageId
 * @export
 * @param {string} id
 * @param {string} lang
 * @param {string} application
 * @returns {Promise<any>}
 */
export function searchImageById(id: string, lang: string, headers: ApiHeader): Promise<any> {
    const uri: string = utils.OPRINT_API_URL + enImgEndpoint + id + '?languageCode=' + lang;
    const config: AxiosRequestConfig = {headers, validateStatus: utils.validateStatus};
    return axios.get(uri, config);
}

export function searchImageByImage(img: Blob, lang: string, headers: ApiHeader): Promise<any> {
    const uri = utils.OPRINT_API_URL + enImgEndpoint;
    const boundary = utils.getBoundary();
    const data = new Blob([
        utils.createFormContent({Language: lang}, 'query', boundary),
        utils.createFileContent('jo.jpg', 'file', 'image/jpeg', boundary),
        img,
        utils.createEndContent(boundary)
    ]);
    const config: AxiosRequestConfig = {
        method: 'POST',
        headers: {
            ...headers,
            'Content-Type': `multipart/form-data; boundary=${boundary}`
        },
        data,
        validateStatus: utils.validateStatus
    };
    return axios(uri, config);
}
