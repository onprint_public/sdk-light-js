import * as EnrichedImage from './enrichedImage';
import * as Picture from './pictures';
import { clickAction } from './click';
export interface ApiHeader {
    ApiKey: string;
    ApplicationName: string;
    ApplicationVersion: string;
    ApplicationInstanceId: string;
    DeviceName: string;
    DeviceSystemVersion: string;
    DeviceSystemVersionName: string;
    SdkVersion: string;
    SdkName: string;
}
export interface AppHeader { name: string; version: string; id: string; }
export interface DevHeader { name: string; version: string; versionName: string; }

export class SdkLight {

    public sessionId: string = '';
    private apiKey: string = '';
    private header: ApiHeader = {
        ApiKey: '',
        SdkVersion: '0.0.4',
        SdkName: 'LTU Tech SDK-Light-JS',
        ApplicationName: '',
        ApplicationVersion: '',
        ApplicationInstanceId: '',
        DeviceName: '',
        DeviceSystemVersion: '',
        DeviceSystemVersionName: '',
    };
    private lang: string = 'fr-FR';

    constructor(apiKey: string, appHeader: AppHeader, devHeader: DevHeader) {
        this.setApiKey(apiKey);
        this.setApplicationHeader(appHeader['name'], appHeader['version'], appHeader['id']);
        this.setDeviceHeader(devHeader['name'], devHeader['version'], devHeader['versionName']);
    }

    public setLang(newLang: string): any {
        if (newLang && newLang.match(/^[a-z]{2}-[A-Z]{2}$/)) {
            this.lang = newLang;
        } else {
            throw new Error('Invalid lang format');
        }
    }

    public setApiKey(key: string) {
        if (!key || typeof key !== 'string') {
            throw new Error('Undefined or invalid API KEY');
        }
        this.apiKey = key;
        this.header['ApiKey'] = this.apiKey;
    }

    public async getEnrichedImageById(id: string): Promise<any> {
        if (!id) {
            throw new Error('Empty image id');
        } else if (typeof id !== 'string') {
            throw new Error('Image id should be a string instead was ' + typeof id);
        }
        try {
            const res = await EnrichedImage.searchImageById(id, this.lang, this.header);
            if (!res || res.status !== 200) {
                return null;
            }
            const enImg = res.data;
            this.sessionId = enImg['SessionId'];
            return enImg;
        } catch (err) {
            throw err;
        }
    }

    public async getEnrichedImageByImage(img: Blob): Promise<any> {
        if (!img) {
            throw new Error('Empty imageData img');
        } else if (typeof img !== 'object') {
            throw new Error('Image should be a File instead was ' + typeof img);
        }
        try {
            const res = await EnrichedImage.searchImageByImage(img, this.lang, this.header);
            if (!res || res.status !== 200) {
                return null;
            }
            const enImg = res.data;
            this.sessionId = enImg['SessionId'];
            return enImg;
        } catch (err) {
            throw err;
        }
    }

    public async getPictureById(id: string): Promise<Picture.PictureObject | null> {
        if (!id) {
            throw new Error('Empty image id');
        } else if (typeof id !== 'string') {
            throw new Error('Image id should be a string instead was ' + typeof id);
        }
        try {
            const res = await Picture.searchImageById(id, this.header);
            if (!res || res.status !== 200) {
                return null;
            }
            const picture = this.jsonToPicture(res.data);
            this.sessionId = picture.sessionId;
            return picture;
        } catch (err) {
            throw err;
        }
    }

    public async getPictureByImage(img: Blob): Promise<Picture.PictureObject[]> {
        if (!img) {
            throw new Error('Empty imageData img');
        } else if (typeof img !== 'object') {
            throw new Error('Image should be a File instead was ' + typeof img);
        }
        try {
            const res = await Picture.searchImageByImage(img, this.header);
            if (!res || res.status !== 200) {
                return [];
            }
            const pictureArray: Picture.PictureObject[] = [];
            for (const picJson of res.data) {
                pictureArray.push(this.jsonToPicture(picJson));
            }
            if (pictureArray[0]) {
                this.sessionId = pictureArray[0].sessionId;
            }
            return pictureArray;
        } catch (err) {
            throw err;
        }
    }

    public async clickOnAction(actionId: string): Promise<any> {
        if (!actionId) {
            throw new Error('Empty action Id');
        } else if (typeof actionId !== 'string') {
            throw new Error('Action id should be a string instead was ' + typeof actionId);
        } else if (!this.sessionId) {
            throw new Error('Undefined SessionId: ' + this.sessionId + 'You should first request an Image');
        }
        try {
            const res = await clickAction(actionId, this.sessionId, this.header);
            if (!res || res.status !== 204) {
                return false;
            }
            return true;
        } catch (err) {
            throw err;
        }
    }

    private jsonToPicture(picJson: any): Picture.PictureObject {
        const picture: Picture.PictureObject = {
            id: picJson['Id'],
            sessionId: picJson['SessionId'],
            orgId: picJson['OrganizationId'],
            score: picJson['RecognitionScore'],
            thumbUrl: picJson['BigThumbnailUrl']
        };
        if (picJson['Name']) { picture.name = picJson['Name']; }
        if (picJson['Data']) { picture.data = picJson['Data']; }
        if (picJson['Keywords']) { picture.keywords = picJson['Keywords']; }
        return picture;
    }

    private setDeviceHeader(name: string, version: string, versionName: string) {
        for (const x in arguments ) {
            if (!arguments[x] || typeof arguments[x] !== 'string') {
                throw new Error ('Invalid arguments type or value to set device header expect string but insted had: ' +
                 arguments[x] + ' in arg ' + x);
            }
        }
        this.header['DeviceName'] = name;
        this.header['DeviceSystemVersion'] = version;
        this.header['DeviceSystemVersionName'] = versionName;
    }

    private setApplicationHeader(name: string, version: string, instanceId: string) {
        for (const x in arguments ) {
            if (!arguments[x] || typeof arguments[x] !== 'string') {
                throw new Error ('Invalid arguments type or value to set application header expect string but insted had: ' +
                 arguments[x] + ' in arg ' + x);
            }
        }
        this.header['ApplicationName'] = name;
        this.header['ApplicationVersion'] = version;
        this.header['ApplicationInstanceId'] = instanceId;
    }
}
