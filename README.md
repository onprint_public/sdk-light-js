# Onprint Javascript SDK Light

This SDK is the easiest way to explore LTU Tech image recognition tools by wraping ONprint API endpoint and provides simple methods to access it

### Instalation
```bash
npm install onprint-sdk-light
```

### Usage
There is two ways to use this SDK:

* With the [SDK Class](#sdk-class-object)
* With the [API wrapped functions](#api-functions)

### **SDK Class Object**
This object contains all the methods to request an Enriched Image, Pictures (by Id or File) and send clickstream action to feed the reporting

#### Initialize SDK

To initialize the SDK you need to provide informations used by our reporting service:
* Device Header
* Application Header
* Api Key

```javascript
import {SdkLight} from 'onprint-sdk-light';

const applicationHeader = {
    name: 'Your Application App Name',
    version: '1.0.0',
    id: 'Random Id' // Unique for each user
};

const deviceHeader = {
    name: 'A Device Name',
    version: '8.0.0',
    versionName: 'Oreo'
};

const sdk = new SdkLight(YOUR_API_KEY, applicationHeader, deviceHeader);
```

By default the lang is set to 'fr-FR' but you can change it with:

```javascript
sdk.setLang('en-EN')
```

more precision with the Lang features here: [ONprint API](http://developer.onprint.com/api.html#scan-of-enriched-images-1)

#### Scan Enriched Image

The ONprint API provides you 2 ways to request [Enriched Images](http://developer.onprint.com/api.html#scan-of-enriched-images-1)

* _With Image Id_

    ```javascript
    try {
        const enrichedImage = await sdk.getEnrichedImageById('b9ceb71c-aad9-4c90-80c6-4fe894de85fb');
    } catch (err) {
        console.log(err);
    }
    ```

* _With Image File_
    ```javascript
    try {
        const imgBuffer = fs.readFileSync('image.jpeg');
        const enrichedImage = await sdk.getEnrichedImageByImage(imageBuffer);
    } catch (err) {
        console.log(err);
    }
    ```

#### Scan Picture

The ONprint API provides you 2 ways to request [Pictures](http://developer.onprint.com/api.html#core-image-recognition-1)

* _With Image Id_

    ```javascript
    try {
        const picture = await sdk.getPictureByImage('b9ceb71c-aad9-4c90-80c6-4fe894de85fb');
    } catch (err) {
        console.log(err);
    }
    ```

* _With Image File_
    ```javascript
    try {
        const imgBuffer = fs.readFileSync('picture.jpeg');
        const pictures = await sdk.getPictureByImage(imageBuffer);
    } catch (err) {
        console.log(err);
    }
    ```


As you can see, the SDK methodes return ```Promise```, if an error occure the SDK will throw an ```Error``` to  catch.

#### Click on Action

This features is really important to get feedback of your user and have data on which content is most used.
To work the Click methode need to get 2 arguments:
    - ActionId find in the EnrichedImages responses Actions attribute
    - SessionId Automaticly set to the last valid Enriched Image request

The methode return ```true``` in case of success

```javascript
try {
    const enrichedImage = await sdk.getEnrichedImageById('b9ceb71c-aad9-4c90-80c6-4fe894de85fb');
    sdk.clickOnAction(enrichedImage.Actions[0].Id);
} catch (err) {
    console.log(err);
}
```

### **API Functions**
If you prefer not use the SDK Class we provide you, it's possible to use directly the Onprint API call and have more controle with the functions return and behavior.

#### Enriched Image

* [Search by Id](https://onprint_public.gitlab.io/sdk-light/modules/_enrichedimage_.html#searchimagebyid)
* [Search by Image](https://onprint_public.gitlab.io/sdk-light/modules/_enrichedimage_.html#searchimagebyimage)

#### Picture

* [Search by Id](https://onprint_public.gitlab.io/sdk-light/modules/_pictures_.html#searchimagebyid)
* [Search by Image](https://onprint_public.gitlab.io/sdk-light/modules/_pictures_.html#searchimagebyimage)

#### Click Stream

* [Click Action](https://onprint_public.gitlab.io/sdk-light/modules/_click_.html#clickaction)

## Test

To run test simply launch the command with your API KEY
```bash
API_KEY=your_api_key npm run test
```

## Support

You can contact me for any issue at bmartins@ltutech.com

## License

© LTU Tech 2018 All Rights Reserved